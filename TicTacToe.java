package com.sg.Extra;

import java.util.Scanner;
import java.util.Random;

public class TicTacToe {

    public static void main(String[] args) {
        System.out.println("Let's play Tic-Tac-Toe!");
        System.out.println(startNewTicTacToeGame());
    }

    private static String startNewTicTacToeGame() {
        printGameInstructions();
        char[][] gameBoard = getEmptyGameBoard();
        for (int turnCounter = 1; turnCounter <= 5; turnCounter++) {
            gameBoard = playerTurn(gameBoard);
            if (isWin(gameBoard, 'X')) {
                return "Well done! You win!";
            }
            if (turnCounter == 5) {
                break;
            }
            gameBoard = computerTurn(gameBoard);
            if (isWin(gameBoard, 'O')) {
                return "The computer won. Better luck next time!";
            }
        }
        return "The game is a draw, how lame.";
    }

    private static void printGameInstructions() {
        System.out.println("In this game, you will be taking turns against the computer.");
        System.out.print("The player who succeeds in placing three of their marks ");
        System.out.println("in a horizontal, vertical, or diagonal row wins the game.");
        System.out.println("You will be Xs and the computer will be Os. You will play first.");
        System.out.print("In order select a space in the 3x3 grid, computer will ");
        System.out.print("first ask you to enter a row number (1-3), ");
        System.out.println("and then it will ask you to enter a column number (1-3)");
        System.out.println("The grid is laid out as follows:");
        System.out.println("    1  2  3");
        System.out.println("1  __|___|__");
        System.out.println("2  __|___|__");
        System.out.println("3    |   |  ");
        System.out.println();
    }

    private static char[][] getEmptyGameBoard() {
        return new char[3][3];
    }

    private static char[][] playerTurn(char[][] gameBoard) {
        int selectedRow, selectedColumn;
        Scanner playerInput = new Scanner(System.in);
        do {
            do {
                System.out.print("Enter the row (1-3) in which you want to place your X: ");
                selectedRow = playerInput.nextInt() - 1;
            } while (selectedRow < 0 || selectedRow > 2);
            do {
                System.out.print("Enter the column (1-3) in which you want to place your X: ");
                selectedColumn = playerInput.nextInt() - 1;
            } while (selectedColumn < 0 || selectedColumn > 2);
        } while (!isSelectedCellEmpty(gameBoard, selectedRow, selectedColumn));
        return updateGameBoard(gameBoard, selectedRow, selectedColumn, 'X');

    }

    private static char[][] computerTurn(char[][] gameBoard) {
        int selectedRow, selectedColumn;
        Random random = new Random();
        do {
            selectedRow = random.nextInt(3);
            selectedColumn = random.nextInt(3);
        } while (!isSelectedCellEmpty(gameBoard, selectedRow, selectedColumn));
        return updateGameBoard(gameBoard, selectedRow, selectedColumn, 'O');
    }

    private static boolean isSelectedCellEmpty(char[][] gameBoard, int gridRow, int gridCol) {
        return (gameBoard[gridRow][gridCol] == 0);
    }

    private static char[][] updateGameBoard(char[][] gameBoard, int gridRow, int gridCol, char mark) {
        gameBoard[gridRow][gridCol] = mark;
        printGameBoard(gameBoard);
        return gameBoard;
    }

    private static void printGameBoard(char[][] gameBoard) {
        for (int i = 1; i <= gameBoard.length; i++) {
            for (int j = 1; j <= gameBoard[i - 1].length; j++) {
                if (i < gameBoard.length && j % 2 == 0) {
                    System.out.print("_|_" + gameBoard[i - 1][j - 1] + "_|_");
                } else if (i == gameBoard.length && j % 2 == 0) {
                    System.out.print(" | " + gameBoard[i - 1][j - 1] + " | ");
                } else {
                    System.out.print(gameBoard[i - 1][j - 1]);
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    private static boolean isWin(char[][] gameBoard, char mark) {
        return ((gameBoard[0][0] == mark && gameBoard[0][1] == mark && gameBoard[0][2] == mark)
                || (gameBoard[1][0] == mark && gameBoard[1][1] == mark && gameBoard[1][2] == mark)
                || (gameBoard[2][0] == mark && gameBoard[2][1] == mark && gameBoard[2][2] == mark)
                || (gameBoard[0][0] == mark && gameBoard[1][0] == mark && gameBoard[2][0] == mark)
                || (gameBoard[0][1] == mark && gameBoard[1][1] == mark && gameBoard[2][1] == mark)
                || (gameBoard[0][2] == mark && gameBoard[1][2] == mark && gameBoard[2][2] == mark)
                || (gameBoard[0][0] == mark && gameBoard[1][1] == mark && gameBoard[2][2] == mark)
                || (gameBoard[2][0] == mark && gameBoard[1][1] == mark && gameBoard[0][2] == mark));
    }

}
